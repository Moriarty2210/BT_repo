# CAN Messages

Die einzelnen Teilnehmer empfangen zwar alle Nachrichten auf dem Bus, 
verarbeiten aber jeweils nur die allgemein gültigen CAN Botschaften und 
den Bereich der für sie interessant ist.
Liste der möglichen CAN Nachrichten:


## Allgemein gültige CAN Botschaften

Der Bereich [Identifier < 10] wird für Nachrichten besonderer Priorität verwendet. Hier können z.B. Error-Nachrichten gesendet und empfangen werden.

| Identifier | Payload |
|---|---|
| 0 | Error Messages |
| 1 | (Heartbeat) |

## Einteilung der Achsen

Die "Zehner"-Stelle des CAN-Identifiers gibt das PCB an (Möglichkeit von ca. 200 einzelnen PCBs)

| Identifier | Info |
|------------|-------|
| 1X | X-Axis |
| 2X | Y-Axis |
| 3X | Z-Axis |

## Einteilung der Funktionen eines PCBs

Die "Einser"-Stelle des CAN-Identifiers gibt die Funktion des PCBs an (Möglichkeit von 10 verschiedenen "Funktion").

| Identifier 	| Payload 	| Info |
|------------	| ---------|---|
| X0 | Konfigurationswert | Konfiguration |
| X1 | 0x00 | Konfigurierungsmodus |
|    | 0x01 | Initiate Axis Reset |
|    | 0x02 | Boardinfo |
| X2 | Positionswert | Sende neue Position |
| X9 | Antwort | Antwortkanal |
