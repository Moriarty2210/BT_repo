function um = steps2um(Axis, steps)
%STEPS2MM Summary of this function goes here
%   Detailed explanation goes here
UMPerSteps = Axis.motor.gearRatio / (Axis.motor.steps * Axis.motor.usteps);
um = round(steps * UMPerSteps);
end

