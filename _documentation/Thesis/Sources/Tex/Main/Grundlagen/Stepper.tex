\subsection{Schrittmotor}
\label{sec:stepper}

\subsubsection{Grundlagen}
Der neben dem Gleichstrommotor zu den Kleinantrieben zählende Schrittmotor zeichnet sich vor allem durch die schrittweise Bewegung aus.
Eine volle Umdrehung der Welle besteht aus einer definierten Anzahl von Einzelschritten. Der Antrieb setzt sich immer aus einer Steuerungslogik, einer Leistungselektronik und dem eigentlichen Motor zusammen (Abb. \ref{stepper}). Die Steuerungslogik stellt eine Impulsfolge für die Leistungselektronik anhand eines Eingangssignals bereit. Die Leistungselektronik generiert aus dieser Impulsfolge Energie für die Motorwicklungen.

Ein einzelner Steuerimpuls sorgt für ein "Weiterschalten" \ des Motors um den Schrittwinkel $\alpha$. Der Motor folgt dieser Schrittwinkelvorgabe mit etwas Verzögerung und verharrt so lange in der neuen Position bis ein neuer Steuerimpuls eintrifft.

\begin{figure}[ht]
	\centering
  \includegraphics[width=0.95\textwidth]{Sources/Pic/stepper.pdf}
	\caption{Antriebsstrang mit Schrittmotor und Last}
	\label{stepper}
\end{figure}

\begin{align}
\label{schrittwinkel}
\alpha = \frac{2 \pi}{z} = \frac{360^{\circ}}{z}
\end{align}

Um den Schrittwinkel $\alpha$ berechnen zu können \eqref{schrittwinkel} wird die Schrittzahl $z$ benötigt, welche vom Motorhersteller fest vorgegeben wird. Die mittlere Drehzahl $N$ des Motors kann über die mechanische Winkelgeschwindigkeit $\Omega_m$ oder durch die Schrittfrequenz $F_z$ berechnet werden \eqref{mittlere_drehzahl}.

\begin{align}
\label{mittlere_drehzahl}
N = \frac{\Omega_m}{2\pi} = \frac{F_z}{z}
\end{align}

Ein Positioniervorgang des Motors erfolgt durch die Aneinanderreihung von Schritten. Dadurch ergibt sich, dass der Gesamtverdrehwinkel des Motors immer ein ganzzahliges Vielfaches von Schrittwinkel $\alpha$ ist. Die diskrete Positionierung des Motors ist durch Zählen der gefahrenen Schritte, ohne weitere Peripherie möglich. \cite[499\psqq]{schröder2013}

\subsubsection{Motortypen}
\paragraph{Reluktanz-Schrittmotor (VR)}
Bei Reluktanz-Schrittmotoren (engl. Variable Reluctance Motors - VR) besteht der Rotor aus einem weichmagnetischen Material, welches sich sehr leicht ummagnetisieren lässt. Der Rotor wird durch das magnetische Feld des Stators magnetisiert und richtet sich danach aus. Dieser Motortyp zeichnet sich durch kleine Schrittwinkel aus. \cite[501\psq]{schröder2013}

\paragraph{Permanentmagneterregter Schrittmotor (PM)}
Bei permanentmagneterregten Schrittmotoren ist der permanent magnetisierte Rotor immer polrichtig zum magnetischen Feld des Stators. Die Drehrichtungsbestimmung erfolgt durch magnetische Polung der Statorpole. Der Motortyp besitzt, im Gegensatz zum VR-Schrittmotor, ein hohes Drehmoment sowie ein Selbsthaltemoment bei nicht erregten Statorwicklungen.\cite[503\psq]{schröder2013}

\paragraph{Hybrid-Schrittmotor (HY)}
Hybrid-Schrittmotoren besitzen einen axial angeordneten Permanentmagenten mit zwei, um halbe Zahnteilung versetzten, Zahnscheiben aus weichmagnetischem Material. Diese Zahnscheiben werden durch den Permanentmagneten magnetisiert und richten sich nach den Statorzähnen aus. Die Statorzähne werden durch umliegende Wicklungen magnetisiert und können so ein Drehfeld ausbilden. Der Motortyp vereint die Vorteile der anderen beiden Schrittmotortypen. Es ist ein hohes Drehmoment und ein Haltemoment vorhanden sowie ein kleiner Schrittwinkel möglich.\cite[505\psq]{schröder2013}

\subsubsection{Betriebsarten}
\paragraph{Vollschrittbetrieb}
Bei Motoren im Vollschrittbetrieb wird immer nur ein einzelner Strang mit dem Strom $I_0$ bestromt. Dadurch ist eine Verkleinerung des Schrittwinkels $\alpha$ nur durch höhere Strangzahl möglich. Somit ergibt sich ein erhöhter Schaltungsaufwand, welcher selten wirtschaftlich ist. Es ist damit $\alpha_{\text{min}} = \alpha$. \cite[519\psq]{schröder2013}

\paragraph{Halbschrittbetrieb}
Im Halbschrittbetrieb werden bis zu zwei nebeneinander liegende Stränge mit dem Strom $I_0$ bestromt. Der Schrittwinkel verkleinert sich hier auf $\frac{\alpha}{2}$. Allerdings schwankt die Anzahl der erregten Stränge bei konstantem Strom $I_0$, wodurch die Amplitude und das Drehmoment schwanken. Dies hat eine Störung der Laufruhe des Motors zur Folge. \cite[520\psqq]{schröder2013}

\paragraph{Mikroschrittbetrieb (Micro-Stepping)}
Durch entsprechende Leistungselektronik können auch Stromwerte zwischen $+I_0$, $-I_0$ und $0$ erzeugt werden. Dies resultiert in einem kleineren Schrittwinkel $\alpha$ und in gesteigerter Laufruhe. Bei geringen Schrittwinkeln kann jedoch die Haftreibung nicht überwunden werden und es kommt zu einem Schrittverlust. Dies ist ein Nachteil dieser Betriebsart. \cite[522]{schröder2013}
