function [] = sendTMCLMessage(targetAdress, TMCLMessageString, canChan)
%SENDTMCLMESSAGE Summary of this function goes here
%   Detailed explanation goes here
tmclData = getTMCLMessage(TMCLMessageString, targetAdress);

message = canMessage(targetAdress, false, 7);
message.Data = ([tmclData(2) tmclData(3) tmclData(4) tmclData(5) tmclData(6) tmclData(7) tmclData(8)]);
transmit(canChan, message);

end

