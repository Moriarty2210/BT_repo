function steps = um2steps(Axis, um)
%STEPS2MM Summary of this function goes here
%   Detailed explanation goes here
stepsPerUM = (Axis.motor.steps * Axis.motor.usteps) / Axis.motor.gearRatio;
steps = floor(um * stepsPerUM);
end


