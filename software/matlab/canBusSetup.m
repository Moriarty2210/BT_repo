function [canChan] = canBusSetup()
%CANBUSSETUP Sets up a CAN Bus
%   Setting up CAN-Bus on Device specified below with the specified Bitrate
deviceManufacturer = 'PEAK-System';
deviceName = 'PCAN_USBBUS1';
bitrate = 1000000;

%% Setting up CanChannel
canChan = canChannel(deviceManufacturer, deviceName);
%% Configuring Bitrate
configBusSpeed(canChan, bitrate);

%% Configuring Callback Function
canChan.MessageReceivedFcn = @canCallback;

%% Starting Channel
% Make shure to stop channel after quitting main-Script

start(canChan);
end

