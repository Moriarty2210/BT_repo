# Anforderungsliste


## Generelle Anforderungen

* Es sollen über einen Bus beliebig viele Achsen angesteuert werden.
* Der Nutzer kann jeder Achse eine beliebige Soll-Position vorgeben.
* Die Achse liefern Rückmeldung über die derzeitige Position.
* Die Achse fährt die gewünschte Position auf 0,1 mm genau an.
* Es kann ein Homing der Achse durchgeführt werden. (Durch Befehl)
	* Die Achse kann über Hardware-Endschalter "genullt" werden.
* Es soll zum kompletten Projekt eine Dokumentation erstellt werden.
* Länge der Linearachse 30-40 cm.


## Sicherheitsanforderungen

* Bei Stromlosigkeit wird der Motor abgeschaltet.
* Bei Wiederherstellung der Spannung läuft der Motor nicht ohne Freigabe alleine an.
* Bei einem Software-Fehler bleibt der Motor stehen.
* Bei einem Software-Halt bleibt der Motor stehen.
* Bei einem Busfehler bleibt der Motor stehen.
* Kraftbegrenzung
* Geschwindigkeitsbegrenzung
* Abschrankungen

## Softwareanforderungen

* Das Board lässt sich über MATLAB ansteuern.
* Das Board lässt sich über kostenlose Software ansteuern:
	* Python
	* Octave


