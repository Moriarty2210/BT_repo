function [binaryForm] = getMessage(input, target)
%GETCANMESSAGE converts TMCL String to CAN Message
%   This function uses a String in the Trinmaic Motor Control Language
%   (TMCL) and converts it to the used Message in the CAN Protocol

%Binary form = [TargetAddr, InstrNumber, Type, Motor/Bank, Value 3, Value 2,
%Value 1, Value 0, Checksum]

%%Generate Command Number from Message
messageTable = ["ROR","ROL","MST","MVP","SAP","GAP","STAP","RSAP","SGP","GGP","STGP","RSGP","RFS","SIO","GIO","CALC","CALC","CALC","CALC","COMP","JC","JA","CSUB","RSUB","EI","DI","WAIT","STOP","SCO","SCO","GCO","CCO","CALCX","AAP","AGP","CLE","VECT","RETI","ACO"];
commandNumber = 0;
for i = 1:39

    if (contains(input,messageTable(i)))
        commandNumber = i;
        break;
    end
end

%% Get additional Content from TMCL Message

%Split workstring on <Space> and erase commata
workStr = erase(split(input, " "), ",");


switch(commandNumber)
    case 1
        %Rotate Right
        motorNumber = str2double(workStr(2));
        velocity = str2double(workStr(3));
        Byte_Array = typecast(int64(velocity), 'uint8');
        type = 0;
        binaryForm = [target, i, type, motorNumber, Byte_Array(4), Byte_Array(3), Byte_Array(2), Byte_Array(1)];
        %Calculate the checksum of table and append checksum
        binaryForm = calcChecksum(binaryForm);
        
    case 2
        %Rotate Left
        motorNumber = str2double(workStr(2));
        velocity = str2double(workStr(3));
        Byte_Array = typecast(int64(velocity), 'uint8');
        type = 0;
        binaryForm = [target, i, type, motorNumber, Byte_Array(4), Byte_Array(3), Byte_Array(2), Byte_Array(1)];
        %Calculate the checksum of table and append checksum
        binaryForm = calcChecksum(binaryForm);
        
    case 3
        %Stop Motor
        motorNumber = str2double(workStr(2));
        type = 0;
        Byte_Array = [0,0,0,0];
        binaryForm = [target, i, type, motorNumber, Byte_Array(4), Byte_Array(3), Byte_Array(2), Byte_Array(1)];
        %Calculate the checksum of table and append checksum
        binaryForm = calcChecksum(binaryForm);
        
    case 4
        positionMode = workStr(2);
        motorNumber = str2double(workStr(3));
        value = str2double(workStr(4));
        Byte_Array = typecast(int64(value), 'uint8');
        switch positionMode
            case char('ABS')
                type = 0;
            case char('REL')
                type = 1;
            case char('COORD')
                type = 2;
        end
        binaryForm = [
            target, i, type, motorNumber, Byte_Array(4), Byte_Array(3), Byte_Array(2), Byte_Array(1)];
        %Calculate the checksum of table and append checksum
        binaryForm = calcChecksum(binaryForm);
        
    case 5
        %Set Axis Parameter
        type = str2double(workStr(2));
        motorNumber = str2double(workStr(3));
        value = str2double(workStr(4));
        Byte_Array = typecast(int64(value), 'uint8');
        binaryForm = [target, i, type, motorNumber, Byte_Array(4), Byte_Array(3), Byte_Array(2), Byte_Array(1)];
        %Calculate the checksum of table and append checksum
        binaryForm = calcChecksum(binaryForm);
        
    case 6
        %Get Axis Paramter
        type = str2double(workStr(2));
        motorNumber = str2double(workStr(3));
        Byte_Array = [0,0,0,0];
        binaryForm = [target, i, type, motorNumber, Byte_Array(4), Byte_Array(3), Byte_Array(2), Byte_Array(1)];
        %Calculate the checksum of table and append checksum
        binaryForm = calcChecksum(binaryForm);
        
    case 7
        parameter = str2double(workStr(2));
        motorNumber = str2double(workStr(3));
        value = str2double(workStr(4));
        
    case 8
        parameter = str2double(workStr(2));
        motorNumber = str2double(workStr(3));
    otherwise
end


end
