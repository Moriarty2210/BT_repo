# BachelorThesis Motordriver Devboard
Dieses Repository enthält alle wichtigen Informationen über das Projekt meiner Bachelorarbeit.

# Generelle Informationen

In der Fakultät für Technik an der Hochschule Pforzheim sollen Motoren in eine Messautomation eingebunden werden.
Mit dieser Messautomation sollen im Anschluss zum Beispiel Magnetfelder im dreidimensionalen Raum um eine Spule gemessen werden.
Dazu müssen die Motoren von einem Computer gesteuert werden, welcher auch die Messautomation übernimmt.

# Projektbeschreibung

Dieses Projekt beschreibt die Entwicklung einer Platine zur Ansteuerung von Motoren sowie zur Rückmeldung von Sensordaten
über ein Bussystem.

# Wiki
Im [Wiki](https://gitlab.com/Moriarty2210/BT_repo/-/wikis/home) findet sich die weitere Dokumentation über das Projekt.