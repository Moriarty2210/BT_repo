function canCallback(canChan)
%CANCALLBACK Function gets all incoming CAN Messages from Bus
%   
global Machine;
canMsg = receive(canChan, 1);

replyAddr = canMsg.ID;
moduleAddr = canMsg.Data(1);
status = canMsg.Data(2);
commandNum = canMsg.Data(3);
value = [canMsg.Data(4), canMsg.Data(5), canMsg.Data(6), canMsg.Data(7)];

switch (replyAddr)
    case 2
        %right board is selected
        switch(commandNum)
            case 3
                if(status == 100)
                    %Motor stopped
                end
            case 4
                if(status == 100)
                    disp("Motor " + moduleAddr + " moving to Position " + value);
                end
            case 6
                if(status == 100)
                   switch (moduleAddr)
                       case 0
                           Machine.axis.x.pos = typecast(value, 'int32');
                       case 1
                       case 2
                       case 3
                       case 4
                   end
                end
            otherwise
        end
    
    otherwise
        disp("ERR: Board not known!");
end


end

