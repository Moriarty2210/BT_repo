function binaryForm = calcChecksum(binaryForm)
%CALCCHECKSUM Summary of this function goes here
%   Detailed explanation goes here

checksum = uint16(0);
for i = 1:8
    checksum = uint16(checksum + uint16(binaryForm(i)));
end
checksum8bit = typecast(checksum, 'uint8');
binaryForm(9) = checksum8bit(1);
end

