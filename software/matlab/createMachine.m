function Machine = createMachine()
%CREATEGLOBALMATRIX Summary of this function goes here
%   Detailed explanation goes here

%%Setting up CAN-Bus interface
Machine.canChan = canBusSetup();

%%Setting up actual Axisposition in mm
Machine.axis.x.pos = 0;
Machine.axis.y.pos = 0;
Machine.axis.z.pos = 0;
Machine.axis.b.pos = 0;
Machine.axis.c.pos = 0;

%%Setting up move-Flag of axis
Machine.axis.x.move = 0;
Machine.axis.y.move = 0;
Machine.axis.z.move = 0;
Machine.axis.b.move = 0;
Machine.axis.c.move = 0;

%%Setting up maximal axis length in mm and for b/c Axis in degrees
Machine.axis.x.length = 200;
Machine.axis.y.length = 200;
Machine.axis.z.length = 200;
Machine.axis.b.length = 90;
Machine.axis.c.length = 350;

%%Setting up last movementdirection
% 1 = movement in the right/up/back direction of the axis
% -1 = movement in the left/down/forward direction of the axis
Machine.axis.x.lastDir = 1;
Machine.axis.y.lastDir = 1;
Machine.axis.z.lastDir = 1;
Machine.axis.b.lastDir = -1;
Machine.axis.c.lastDir = -1;

%%Defining Machine parameters
%X-Axis
Machine.axis.x.motor.steps = 600;
Machine.axis.x.motor.usteps = 32;
Machine.axis.x.motor.gearRatio = 3500;
Machine.axis.x.motor.stepsChangeDirPos = 0;
Machine.axis.x.motor.stepsChangeDirNeg = 0;
Machine.axis.x.motor.StepsAxisLoss = 0;


end

