function [AxisRet] = moveAxis(Axis, canChan, position_um)
%MOVEAXIS move the Axis to the position in um
AxisRet = Axis;
%% Parameters
uSteps = Axis.motor.usteps;
MotorSteps = Axis.motor.steps;
gearRatio = Axis.motor.gearRatio;
stepsUmkehrPos = Axis.motor.stepsChangeDirPos;
stepsUmkehrNeg = Axis.motor.stepsChangeDirNeg;
microStepsAxisLoss = Axis.motor.StepsAxisLoss;

if(position_um == Axis.pos)
    return
end

%% Step Calculation
DestMicroSteps = -um2steps(Axis, position_um);
lastDir = Axis.lastDir;

if (position_um > Axis.pos)
    currentDir = -1;
else
    currentDir = 1;
end

if(lastDir ~= currentDir)
    %Direction has changed: Add Axis change direction steps
    if(currentDir > 0)
        DestMicroSteps = DestMicroSteps + stepsUmkehrPos;
    else
        DestMicroSteps = DestMicroSteps - stepsUmkehrNeg;
    end
    AxisRet.lastDir = currentDir;
else
    %Direction has not changed: Add nothing to Axis direction steps
end

MicroStepsArray = typecast(int64(DestMicroSteps),'int8');
MicroStepsArray = typecast(MicroStepsArray, 'uint8');

%% Fix: do not use RAW CAN-Handling!
canMsg = canMessage(1,false,7);
canMsg.Data = [4, 0, 0, MicroStepsArray(4),MicroStepsArray(3), MicroStepsArray(2), MicroStepsArray(1)];
transmit(canChan, canMsg);




end

